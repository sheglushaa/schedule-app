package sc.server.core;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.Driver;

public class DatabaseManager {
    private static final Class<? extends Driver> DRIVER_CLASS = com.mysql.jdbc.Driver.class;
    private static final String URL = "jdbc:mysql://localhost:3306/schedule" +
            "?useUnicode=true" +
            "&characterEncoding=UTF-8" +
            "&character_set_client=UTF-8" +
            "&character_set_database=UTF-8" +
            "&character_set_results=UTF8" +
            "&character_set_server=UTF-8" +
            "&character_set_system=UTF-8";

    private static final String USERNAME = "root";
    private static final String PASSWORD = "";


    public static void init() {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(DRIVER_CLASS);
        dataSource.setUsername(USERNAME);
        dataSource.setUrl(URL);
        dataSource.setPassword(PASSWORD);

        jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.execute("SET NAMES 'UTF8'");
        jdbcTemplate.execute("SET CHARACTER SET 'UTF8'");
    }

    private static JdbcTemplate jdbcTemplate = null;

    public static JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
