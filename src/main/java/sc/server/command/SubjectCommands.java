package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Faculty;
import sc.server.model.Subject;
import sc.server.repository.SubjectRepository;

import java.util.List;

@ShellComponent()
public class SubjectCommands {
    private SubjectRepository subjectRepository = Storage.getSubjectRepository();

    @ShellMethod("Get all subjects")
    public void getallsubjects() {
        List<Subject> subjects = subjectRepository.findAll();
        for (Subject it : subjects)
            System.out.println(it.toString());
    }

    @ShellMethod("Add new subject")
    public void addsubject(String name, Integer hours) {
        Subject subject = subjectRepository.create(new Subject(name, hours));
        if (subject == null) {
            System.out.println("Can't create subject");
            return;
        }

        System.out.println("Created subject with id: " + subject.getId());
    }
}
