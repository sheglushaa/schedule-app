package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Account;
import sc.server.model.Group;
import sc.server.model.Lesson;
import sc.server.model.Subject;
import sc.server.repository.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@ShellComponent()
public class LessonCommands {
    private GroupRepository groupRepository = Storage.getGroupRepository();
    private SubjectRepository subjectRepository = Storage.getSubjectRepository();
    private ClassRepository classRepository = Storage.getClassRepository();
    private LessonRepository lessonRepository = Storage.getLessonRepository();
    private AccountRepository accountRepository = Storage.getAccountRepository();

    @ShellMethod("Get all lessons")
    public void getalllessons() {
        List<Lesson> classes = lessonRepository.findAll();
        for (Lesson it : classes)
            System.out.println(it.toString());
    }

    @ShellMethod("Add new lesson")
    public void addlesson(String dateStr, int subjectId, int groupId, int teacherId, int classId, boolean isExam) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy/HH:mm");
        Date date;
        try {
            date = formatter.parse(dateStr);
            System.out.println(date);
            System.out.println(formatter.format(date));
        } catch (ParseException e) {
            System.out.println("Incorrect date");
            return;
        }

        Subject subject = subjectRepository.findById(subjectId);
        if (subject == null) {
            System.out.println("Subject not found");
            return;
        }

        Group group = groupRepository.findById(groupId);
        if (group == null) {
            System.out.println("Subject not found");
            return;
        }

        sc.server.model.Class lClass = classRepository.findById(classId);
        if (lClass == null) {
            System.out.println("Class not found");
            return;
        }

        Account teacher = accountRepository.findById(teacherId);
        if (teacher == null) {
            System.out.println("Teacher not found");
            return;
        }

        if (!teacher.isTeacher()) {
            System.out.println("Provided account is not teacher");
            return;
        }


        Lesson lesson = lessonRepository.create(new Lesson(subject, group, teacher, lClass, date, isExam));
        if (lesson == null) {
            System.out.println("Can't create lesson");
            return;
        }

        System.out.println("Created lesson with id: " + lesson.getId());
    }

    @ShellMethod("Get group lessons")
    public void getgrouplessons(int groupId) {
        Group group = groupRepository.findById(groupId);
        if (group == null) {
            System.out.println("Section not found");
            return;
        }

        List<Lesson> lessonList = lessonRepository.findByGroup(group);
        for (Lesson it : lessonList) {
            System.out.println(it);
        }
    }

    @ShellMethod("Get teacher classes")
    public void getteacherclasses(int teacherId) {
        Account teacher = accountRepository.findById(teacherId);
        if (teacher == null) {
            System.out.println("Teacher not found");
            return;
        }

        List<Lesson> lessonList = lessonRepository.findByTeacher(teacher);
        for (Lesson it : lessonList) {
            System.out.println(it);
        }
    }
}
