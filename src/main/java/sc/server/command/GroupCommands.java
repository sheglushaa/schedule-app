package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Faculty;
import sc.server.model.Group;
import sc.server.repository.FacultyRepository;
import sc.server.repository.GroupRepository;

import java.util.List;

@ShellComponent()
public class GroupCommands {
    private GroupRepository groupRepository = Storage.getGroupRepository();
    private FacultyRepository facultyRepository = Storage.getFacultyRepository();

    @ShellMethod("Get all groups")
    public void getallgroups() {
        List<Group> groups = groupRepository.findAll();
        for (Group it : groups)
            System.out.println(it.toString());
    }

    @ShellMethod("Add new group")
    public void addgroup(String name, Integer facultyId) {
        Faculty faculty = facultyRepository.findById(facultyId);
        if (faculty == null) {
            System.out.println("Can't found faculty");
            return;
        }

        Group group = groupRepository.create(new Group(name, faculty));
        if (group == null) {
            System.out.println("Can't create group");
            return;
        }

        System.out.println("Created group with id: " + group.getId());
    }
}
