package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Faculty;
import sc.server.repository.FacultyRepository;

import java.util.List;

@ShellComponent()
public class FacultyCommands {
    private FacultyRepository facultyRepository = Storage.getFacultyRepository();

    @ShellMethod("Get all faculties")
    public void getallfaculties() {
        List<Faculty> faculties = facultyRepository.findAll();
        for (Faculty it : faculties)
            System.out.println(it.toString());
    }

    @ShellMethod("Add new faculty")
    public void addfaculty(String number, String name) {
        Faculty faculty = facultyRepository.create(new Faculty(number, name));
        if (faculty == null) {
            System.out.println("Can't create faculty");
            return;
        }

        System.out.println("Created faculty with id: " + faculty.getId());
    }
}
