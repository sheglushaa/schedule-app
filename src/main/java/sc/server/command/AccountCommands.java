package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Account;
import sc.server.model.Session;
import sc.server.repository.AccountRepository;
import sc.server.repository.SessionRepository;

import java.util.List;

@ShellComponent()
public class AccountCommands {
    private AccountRepository accountRepository = Storage.getAccountRepository();
    private SessionRepository sessionRepository = Storage.getSessionRepository();

    @ShellMethod("Login to account")
    public void login(String login, String password) {
        Account account = accountRepository.findByLogin(login);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        if (!account.isPasswordValid(password)) {
            System.out.println("Password is not valid");
            return;
        }

        Storage.session = sessionRepository.create(account);
        System.out.println("Logged in: " + account.toString());
    }

    @ShellMethod("Logout from account")
    public void logout() {
        if (Storage.session == null) {
            System.out.println("You are not logged in");
            return;
        }

        Storage.session = null;
        System.out.println("Logged out");
    }

    @ShellMethod("Get account by id")
    public void getaccount(Integer id) {
        Account account = accountRepository.findById(id);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        System.out.println(account.toString());
    }

    @ShellMethod("Get all accounts")
    public void getallaccounts() {
        List<Account> accounts = accountRepository.findAll();
        for (Account account : accounts)
            System.out.println(account.toString());
    }

    @ShellMethod("Set account password by id")
    public void setaccountpassword(Integer id, String password) {
        Account account = accountRepository.findById(id);

        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        account.setPassword(password);
        accountRepository.save(account);
        System.out.println("Password set for " + account.toString());
    }

    @ShellMethod("Set account teacher")
    public void setaccountteacher(Integer id, Boolean isTeacher) {
        Account account = accountRepository.findById(id);

        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        account.setTeacher(isTeacher);
        accountRepository.save(account);
        System.out.println("Updated: " + account.toString());
    }

    @ShellMethod("Set account admin")
    public void setaccountadmin(Integer id, Boolean isAdmin) {
        Account account = accountRepository.findById(id);

        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        account.setAdmin(isAdmin);
        accountRepository.save(account);
        System.out.println("Updated: " + account.toString());
    }

    @ShellMethod("Delete account by id")
    public void deleteaccount(Integer id) {
        Account account = accountRepository.findById(id);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        accountRepository.delete(account);
        System.out.println("Successfully deleted " + account.toString());
    }

    @ShellMethod("Get account type by id")
    public void getaccounttype(Integer id) {
        Account account = accountRepository.findById(id);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        if (account.isTeacher() && account.isAdmin()) {
            System.out.println("Account is teacher and admin");
            return;
        }

        if (account.isTeacher()) {
            System.out.println("Account is teacher");
            return;
        }

        if (account.isAdmin()) {
            System.out.println("Account is admin");
            return;
        }

        System.out.println("Account: Who am I???");
    }

    @ShellMethod("Get account info")
    public void getaccountinfo(Integer id) {
        Account account = accountRepository.findById(id);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }
        System.out.println(account.toString());
    }

}
