package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Class;
import sc.server.repository.ClassRepository;

import java.util.List;

@ShellComponent
public class ClassCommands {
    private ClassRepository classRepository = Storage.getClassRepository();

    @ShellMethod("Get all classes")
    public void getallclasses() {
        List<Class> classes = classRepository.findAll();
        for (Class it : classes)
            System.out.println(it.toString());
    }

    @ShellMethod("Add new class")
    public void addclass(String number, int seats, String block, String department) {
        Class createdClass = classRepository.create(new Class(number, seats, block, department));
        if (createdClass == null) {
            System.out.println("Can't create class");
            return;
        }

        System.out.println("Created class with id: " + createdClass.getId());
    }
}
