package sc.server;

import sc.server.model.Session;
import sc.server.repository.*;

public class Storage {
    public static AccountRepository getAccountRepository() { return accountRepository; }
    public static ClassRepository getClassRepository() { return classRepository; }
    public static FacultyRepository getFacultyRepository() { return facultyRepository; }
    public static GroupRepository getGroupRepository() { return groupRepository; }
    public static LessonRepository getLessonRepository() { return lessonRepository; }
    public static SessionRepository getSessionRepository() {
        return sessionRepository;
    }
    public static SubjectRepository getSubjectRepository() { return subjectRepository; }

    private static final AccountRepository accountRepository = new AccountRepository();
    private static final ClassRepository classRepository = new ClassRepository();
    private static final FacultyRepository facultyRepository = new FacultyRepository();
    private static final GroupRepository groupRepository = new GroupRepository();
    private static final LessonRepository lessonRepository = new LessonRepository();
    private static final SessionRepository sessionRepository = new SessionRepository();
    private static final SubjectRepository subjectRepository = new SubjectRepository();

    public static Session session = null;
}
