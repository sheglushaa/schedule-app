package sc.server.model;

public class Class {
    public Integer id = null;
    private String number;
    private Integer seats;
    private String block;
    private String department;

    public Class(Integer id, String number, Integer seats, String block, String department) {
        this.id = id;
        this.number = number;
        this.seats = seats;
        this.block = block;
        this.department = department;
    }

    public Class(String number, Integer seats, String block, String department) {
        this.number = number;
        this.seats = seats;
        this.block = block;
        this.department = department;
    }

    public Integer getId() { return id; }

    public String getNumber() {
        return number;
    }

    public Integer getSeats() {
        return seats;
    }

    public String getBlock() {
        return block;
    }

    public String getDepartment() {
        return department;
    }

    public boolean isCreated() { return id != null; }

    @Override
    public String toString() {
        return "Class " +
                "id: " + id +
                ", number: " + number +
                ", seats: " + seats +
                ", block: " + block +
                ", department: " + department;
    }
}
