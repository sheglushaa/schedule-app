package sc.server.model;


import java.util.Date;

public class Lesson {
    private Integer id = null;
    private Subject subject;
    private Group group;
    private Account teacher;
    private Class lClass;
    private Date date;
    private boolean isExam;

    public Lesson(Integer id, Subject subject, Group group, Account teacher, Class lClass, Date date, boolean isExam) {
        this.id = id;
        this.subject = subject;
        this.group = group;
        this.teacher = teacher;
        this.lClass = lClass;
        this.date = date;
        this.isExam = isExam;
    }

    public Lesson(Subject subject, Group group, Account teacher, Class lClass, Date date, boolean isExam) {
        this.subject = subject;
        this.group = group;
        this.teacher = teacher;
        this.lClass = lClass;
        this.date = date;
        this.isExam = isExam;
    }

    public Integer getId() { return id; }

    public boolean isCreated() { return id != null; }

    public Subject getSubject() {
        return subject;
    }

    public Group getGroup() {
        return group;
    }

    public Account getTeacher() {
        return teacher;
    }

    public Class getlClass() {
        return lClass;
    }

    public Date getDate() {
        return date;
    }

    public boolean isExam() {
        return isExam;
    }

    @Override
    public String toString() {
        return "Lesson " +
                "id: " + id +
                ", date: " + date +
                ", subject: " + subject.getName() +
                ", group: " + group.getName() +
                ", teacher: " + teacher.getName() +
                ", class: " + lClass.toString() +
                ", isExam: " + isExam();
    }
}
