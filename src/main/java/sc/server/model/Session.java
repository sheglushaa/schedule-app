package sc.server.model;

public class Session {
    private String token;
    private Integer accountId;

    public Session(String token, Integer accountId) {
        this.token = token;
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "Session " +
                "token: " + token +
                ", accountId: " + accountId;
    }
}
