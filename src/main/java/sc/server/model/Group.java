package sc.server.model;

public class Group {
    private Integer id = null;
    private String name;
    private Faculty faculty;

    public Group(int id, String name, Faculty faculty) {
        this.id = id;
        this.name = name;
        this.faculty = faculty;
    }

    public Group(String name, Faculty faculty) {
        this.name = name;
        this.faculty = faculty;
    }

    public Integer getId() { return id; }

    public String getName() { return name; }

    public Faculty getFaculty() { return faculty; }

    public boolean isCreated() { return id != null; }

    @Override
    public String toString() {
        return "Group " +
                "id: " + id +
                ", name: " + name +
                " (faculty " + faculty.toString() + ")";
    }
}
