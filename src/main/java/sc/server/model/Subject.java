package sc.server.model;

public class Subject {
    private Integer id = null;
    private String name;
    private Integer hours;

    public Subject(Integer id, String name, Integer hours) {
        this.id = id;
        this.name = name;
        this.hours = hours;
    }

    public Subject(String name, Integer hours) {
        this.name = name;
        this.hours = hours;
    }

    public Integer getId() { return id; }

    public String getName() { return name; }

    public Integer getHours() {
        return hours;
    }

    public boolean isCreated() { return id != null; }

    @Override
    public String toString() {
        return "Subject " +
                "id: " + id +
                ", name: " + name +
                ", hours: " + hours;
    }
}
