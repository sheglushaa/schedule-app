package sc.server.model;

public class Faculty {
    public Integer id = null;
    private String name;
    private String number;

    public Faculty(int id, String number, String name) {
        this.id = id;
        this.number = number;
        this.name = name;
    }

    public Faculty(String number, String name) {
        this.number = number;
        this.name = name;
    }

    public Integer getId() { return id; }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public boolean isCreated() { return id != null; }

    @Override
    public String toString() {
        return "Faculty " +
                "id: " + id +
                ", name: " + name +
                ", number: " + number;
    }
}
