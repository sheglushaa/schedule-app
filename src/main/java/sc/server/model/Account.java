package sc.server.model;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;

public class Account {
    private Integer id = null;
    private String login;
    private String name;
    private String title;
    private String passwordHash;
    private String passwordSalt;
    private boolean isTeacher;
    private boolean isAdmin;

    public Account(Integer id, String login, String name, String title, String passwordHash, String passwordSalt, boolean isTeacher, boolean isAdmin) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.title = title;
        this.passwordHash = passwordHash;
        this.passwordSalt = passwordSalt;
        this.isTeacher = isTeacher;
        this.isAdmin = isAdmin;
    }

    public Account(String login, String password, String name, String title, boolean isTeacher, boolean isAdmin) {
        this.login = login;
        this.name = name;
        this.title = title;
        this.isTeacher = isTeacher;
        this.isAdmin = isAdmin;

        this.setPassword(password);
    }

    public static boolean isValidName(String name) {
        return name.matches("[А-ЯЁ][а-яё]*([-][А-ЯЁ][а-яё]*)?\\s[А-ЯЁ][а-яё]*\\s[А-ЯЁ][а-яё]*+\\.?");
    }

    public Integer getId() {
        return id;
    }

    public boolean isTeacher() {
        return this.isTeacher;
    }

    public boolean isAdmin() {
        return this.isAdmin;
    }

    public String getTitle() { return title; }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public boolean isPasswordValid(String password) {
        return DigestUtils.sha512Hex(password + passwordSalt).equalsIgnoreCase(passwordHash);
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public void setPassword(String password) {
        this.passwordSalt = RandomStringUtils.randomAlphabetic(8);
        this.passwordHash = DigestUtils.sha512Hex(password + this.passwordSalt);
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public void setTeacher(boolean isTeacher) {
        this.isTeacher = isTeacher;
    }

    public boolean isCreated() {
        return id != null;
    }

    @Override
    public String toString() {
        return "Account " +
                "id: " + id +
                ", name: " + name +
                ", title: " + title +
                ", login: " + login +
                ", isTeacher: " + isTeacher +
                ", isAdmin: " + isAdmin +
                ", password hash: " + passwordHash +
                ", password salt: " + passwordSalt
                ;
    }
}
