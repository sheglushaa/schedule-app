package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.*;
import sc.server.model.Class;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LessonRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Lesson> ROW_MAPPER_CLASS = (ResultSet resultSet, int rowNum) -> {
        Class lClass = new Class(
            resultSet.getInt("class_id"),
            resultSet.getString("class_number"),
            resultSet.getInt("class_seats"),
            resultSet.getString("class_block"),
            resultSet.getString("class_department")
        );

        Group group = new Group(
            resultSet.getInt("group_id"),
            resultSet.getString("group_name"),
            new Faculty(
                resultSet.getInt("faculty_id"),
                resultSet.getString("faculty_number"),
                resultSet.getString("faculty_name")
            )
        );

        Account teacher = new Account(
                resultSet.getInt("teacher_id"),
                resultSet.getString("teacher_login"),
                resultSet.getString("teacher_name"),
                resultSet.getString("teacher_title"),
                resultSet.getString("teacher_password_hash"),
                resultSet.getString("teacher_password_salt"),
                resultSet.getInt("teacher_is_teacher") == 1,
                resultSet.getInt("teacher_is_admin") == 1
        );

        Subject subject = new Subject(
                resultSet.getInt("subject_id"),
                resultSet.getString("subject_name"),
                resultSet.getInt("subject_hours")
        );

        return new Lesson(
                resultSet.getInt("id"),
                subject,
                group,
                teacher,
                lClass,
                resultSet.getDate("date"),
                resultSet.getInt("is_exam") == 1
        );
    };

    public List<Lesson> findAll() {
        return jdbcTemplate.query("SELECT lessons.*,\n" +
                "       g.id as group_id, g.name as group_name,\n" +
                "       f.id as facility_name, f.number as facility_number, f.name as facility_name,\n" +
                "       a.id as teacher_id, a.login as teacher_login, a.name as teacher_name, a.title as teacher_title, a.password_hash as teacher_password_hash, a.password_salt as teacher_password_salt, a.is_teacher as teacher_is_teacher, a.is_admin as teacher_is_admin,\n" +
                "       s.id as subject_id, s.name as subject_name, s.hours as subject_hours\n" +
                "FROM `lessons` LEFT JOIN `groups` g on lessons.group_id = g.id LEFT JOIN accounts a on lessons.teacher_id = a.id LEFT JOIN faculties f on g.faculty_id = f.id LEFT JOIN subjects s on lessons.subject_id = s.id LEFT JOIN classes c on lessons.class_id = c.id", ROW_MAPPER_CLASS);
    }

    public Lesson findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("SELECT l.*,\n" +
                    "       g.id as group_id, g.name as group_name,\n" +
                    "       f.id as facility_name, f.number as facility_number, f.name as facility_name,\n" +
                    "       a.id as teacher_id, a.login as teacher_login, a.name as teacher_name, a.title as teacher_title, a.password_hash as teacher_password_hash, a.password_salt as teacher_password_salt, a.is_teacher as teacher_is_teacher, a.is_admin as teacher_is_admin,\n" +
                    "       s.id as subject_id, s.name as subject_name, s.hours as subject_hours\n" +
                    "FROM `lessons` l LEFT JOIN `groups` g on l.group_id = g.id LEFT JOIN accounts a on l.teacher_id = a.id LEFT JOIN faculties f on g.faculty_id = f.id LEFT JOIN subjects s on l.subject_id = s.id LEFT JOIN classes c on l.class_id = c.id WHERE l.id = ?", new Object[]{id}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public List<Lesson> findByGroup(Group group) {
        try {
            return jdbcTemplate.query("SELECT l.*,\n" +
                    "       g.id as group_id, g.name as group_name,\n" +
                    "       f.id as facility_name, f.number as facility_number, f.name as facility_name,\n" +
                    "       a.id as teacher_id, a.login as teacher_login, a.name as teacher_name, a.title as teacher_title, a.password_hash as teacher_password_hash, a.password_salt as teacher_password_salt, a.is_teacher as teacher_is_teacher, a.is_admin as teacher_is_admin,\n" +
                    "       s.id as subject_id, s.name as subject_name, s.hours as subject_hours\n" +
                    "FROM `lessons` l LEFT JOIN `groups` g on l.group_id = g.id LEFT JOIN accounts a on l.teacher_id = a.id LEFT JOIN faculties f on g.faculty_id = f.id LEFT JOIN subjects s on l.subject_id = s.id LEFT JOIN classes c on l.class_id = c.id WHERE g.id = ?", new Object[]{group.getId()}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return new ArrayList<>();
        }
    }

    public List<Lesson> findByTeacher(Account teacher) {
        Integer id = teacher.getId();

        try {
            return jdbcTemplate.query("SELECT l.*,\n" +
                    "       g.id as group_id, g.name as group_name,\n" +
                    "       f.id as facility_name, f.number as facility_number, f.name as facility_name,\n" +
                    "       a.id as teacher_id, a.login as teacher_login, a.name as teacher_name, a.title as teacher_title, a.password_hash as teacher_password_hash, a.password_salt as teacher_password_salt, a.is_teacher as teacher_is_teacher, a.is_admin as teacher_is_admin,\n" +
                    "       s.id as subject_id, s.name as subject_name, s.hours as subject_hours\n" +
                    "FROM `lessons` l LEFT JOIN `groups` g on l.group_id = g.id LEFT JOIN accounts a on l.teacher_id = a.id LEFT JOIN faculties f on g.faculty_id = f.id LEFT JOIN subjects s on l.subject_id = s.id LEFT JOIN classes c on l.class_id = c.id WHERE a.id = ?", new Object[]{id}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return new ArrayList<>();
        }
    }

    public Lesson create(Lesson lesson) {
        if (lesson.isCreated()) {
            return null;
        }

        Date date = lesson.getDate();
        Class lClass = lesson.getlClass();
        Account teacher = lesson.getTeacher();
        Subject subject = lesson.getSubject();
        Group group = lesson.getGroup();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO lessons SET subject_id = ?, group_id = ?, teacher_id = ?, class_id = ?, date = ?, is_exam = ?");
            ps.setInt(1, subject.getId());
            ps.setInt(2, group.getId());
            ps.setInt(3, teacher.getId());
            ps.setInt(4, lClass.getId());
            ps.setTimestamp(5, new java.sql.Timestamp(date.getTime()));
            ps.setInt(6, lesson.isExam() ? 1 : 0);
            return ps;
        }, keyHolder);

        Integer id = Math.toIntExact((long)keyHolder.getKey());

        return id == null ? null : new Lesson(
                id,
                subject,
                group,
                teacher,
                lClass,
                date,
                lesson.isExam()
        );
    }
}
