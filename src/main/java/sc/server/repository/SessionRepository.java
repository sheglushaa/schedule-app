package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Account;
import sc.server.model.Session;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

public class SessionRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Session> ROW_MAPPER_SESSION = (ResultSet resultSet, int rowNum) -> new Session(
            resultSet.getString("token"),
            resultSet.getInt("account_id")
    );

    public List<Session> findAll() {
        return jdbcTemplate.query("SELECT * FROM sessions", ROW_MAPPER_SESSION);
    }

    public Session create(Account account) {
        String token = Long.toHexString(new Date().getTime());
        Integer accountId = account.getId();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO sessions SET token = ?, account_id = ?");
            ps.setString(1, token);
            ps.setInt(2, accountId);
            return ps;
        }, keyHolder);

        return new Session(token, accountId);
    }

    public int deleteAll() {return jdbcTemplate.update("DELETE FROM sessions;"); }
}
