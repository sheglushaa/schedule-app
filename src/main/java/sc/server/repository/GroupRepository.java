package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Faculty;
import sc.server.model.Group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class GroupRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Group> ROW_MAPPER_CLASS = (ResultSet resultSet, int rowNum) -> new Group(
        resultSet.getInt("id"),
        resultSet.getString("name"),
        new Faculty(
                resultSet.getInt("faculty_id"),
                resultSet.getString("faculty_number"),
                resultSet.getString("faculty_name")
        )
    );

    public List<Group> findAll() {
        return jdbcTemplate.query("SELECT *, f.name as faculty_name, f.number as faculty_number FROM `groups` g LEFT JOIN faculties f on g.faculty_id = f.id", ROW_MAPPER_CLASS);
    }

    public Group findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("SELECT *, f.name as faculty_name, f.number as faculty_number FROM `groups` g LEFT JOIN faculties f on g.faculty_id = f.id WHERE g.id = ? ", new Object[]{id}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public Group create(Group group) {
        if (group.isCreated()) {
            return null;
        }

        String name = group.getName();
        Integer facultyId = group.getFaculty().id;

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO `groups` SET name = ?, faculty_id = ?");
            ps.setString(1, name);
            ps.setInt(2, facultyId);
            return ps;
        }, keyHolder);

        Integer id = Math.toIntExact((long)keyHolder.getKey());
        return id == null ? null : new Group(id, name, group.getFaculty());
    }
}
