package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Class;
import sc.server.model.Subject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class SubjectRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Subject> ROW_MAPPER_CLASS = (ResultSet resultSet, int rowNum) -> new Subject(
        resultSet.getInt("id"),
        resultSet.getString("name"),
        resultSet.getInt("hours")
    );

    public List<Subject> findAll() {
        return jdbcTemplate.query("SELECT * FROM subjects", ROW_MAPPER_CLASS);
    }

    public Subject findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM subjects WHERE id = ?", new Object[]{id}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public Subject create(Subject subject) {
        if (subject.isCreated()) {
            return null;
        }

        KeyHolder keyHolder = new GeneratedKeyHolder();

        String name = subject.getName();
        Integer hours = subject.getHours();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO subjects SET name = ?, hours = ?");
                    ps.setString(1, name);
                    ps.setInt(2, hours);
            return ps;
        }, keyHolder);

        Integer id = Math.toIntExact((long)keyHolder.getKey());
        return id == null ? null : new Subject(id, name, hours);
    }
}
