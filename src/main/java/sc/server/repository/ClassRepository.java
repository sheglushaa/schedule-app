package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.*;
import sc.server.model.Class;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClassRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Class> ROW_MAPPER_CLASS = (ResultSet resultSet, int rowNum) -> new Class(
        resultSet.getInt("id"),
        resultSet.getString("number"),
        resultSet.getInt("seats"),
        resultSet.getString("block"),
        resultSet.getString("department")
    );

    public List<Class> findAll() {
        return jdbcTemplate.query("SELECT * FROM classes", ROW_MAPPER_CLASS);
    }

    public Class findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM classes WHERE id = ?", new Object[]{id}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public Class create(Class classToCreate) {
        if (classToCreate.isCreated()) {
            return null;
        }

        String number = classToCreate.getNumber();
        Integer seats = classToCreate.getSeats();
        String block = classToCreate.getBlock();
        String department = classToCreate.getDepartment();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO classes SET number = ?, seats = ?, block = ?, department = ?");
            ps.setString(1, number);
            ps.setInt(2, seats);
            ps.setString(3, block);
            ps.setString(4, department);
            return ps;
        }, keyHolder);

        Integer id = Math.toIntExact((long)keyHolder.getKey());
        return id == null ? null : new Class(id, number, seats, block, department);
    }
}
