package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Class;
import sc.server.model.Faculty;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class FacultyRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Faculty> ROW_MAPPER_CLASS = (ResultSet resultSet, int rowNum) -> new Faculty(
            resultSet.getInt("id"),
            resultSet.getString("number"),
            resultSet.getString("name")
    );

    public List<Faculty> findAll() {
        return jdbcTemplate.query("SELECT * FROM faculties", ROW_MAPPER_CLASS);
    }

    public Faculty findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM faculties WHERE id = ?", new Object[]{id}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public Faculty create(Faculty faculty) {
        if (faculty.isCreated()) {
            return null;
        }

        String number = faculty.getNumber();
        String name = faculty.getName();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO faculties SET number = ?, name = ?");
            ps.setString(1, number);
            ps.setString(2, name);
            return ps;
        }, keyHolder);

        Integer id = Math.toIntExact((long)keyHolder.getKey());

        return id == null ? null : new Faculty(id, number, name);
    }
}
