package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Account;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;


public final class AccountRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Account> ROW_MAPPER = (ResultSet resultSet, int rowNum) -> new Account(
        resultSet.getInt("id"),
        resultSet.getString("login"),
        resultSet.getString("name"),
        resultSet.getString("title"),
        resultSet.getString("password_hash"),
        resultSet.getString("password_salt"),
        resultSet.getInt("is_teacher") == 1,
        resultSet.getInt("is_admin") == 1
    );

    public List<Account> findAll() {
        return jdbcTemplate.query("SELECT * FROM accounts", ROW_MAPPER);
    }
    
    public Account findById(int id) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM accounts WHERE id = ?", new Object[]{id}, ROW_MAPPER);
        } catch (Exception dataAccessException) {
            return null;
        }

    }

    // Вернет 1, если аккаунт сохранен, 0, если нет
    public void save(Account account) {
        if (!account.isCreated()) {
            this.create(account);
            return;
        }

        jdbcTemplate.update("UPDATE accounts SET login = ?, name = ?, title = ?, password_hash = ?, password_salt = ?, is_teacher = ?, is_admin = ? WHERE id = ?",
            account.getLogin(),
            account.getName(),
            account.getTitle(),
            account.getPasswordHash(),
            account.getPasswordSalt(),
            account.isTeacher() ? 1 : 0,
            account.isAdmin() ? 1 : 0,
            account.getId()
        );
    }

    public Account create(Account account) {
        if (account.isCreated()) {
            return null;
        }

        String login = account.getLogin();
        String name = account.getName();
        String title = account.getTitle();
        String passwordHash = account.getPasswordHash();
        String passwordSalt = account.getPasswordSalt();
        Integer isTeacher = account.isTeacher() ? 1 : 0;
        Integer isAdmin = account.isAdmin() ? 1 : 0;

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO accounts SET login = ?, name = ?, title = ?, password_hash = ?, password_salt = ?, is_teacher = ?, is_admin = ?");
            ps.setString(1, login);
            ps.setString(2, name);
            ps.setString(3, title);
            ps.setString(4, passwordHash);
            ps.setString(4, passwordSalt);
            ps.setInt(5, isTeacher);
            ps.setInt(6, isAdmin);
            return ps;
        }, keyHolder);

        Integer id = Math.toIntExact((long)keyHolder.getKey());
        return id == null ? null : new Account(id, login, name, title, passwordHash, passwordSalt, account.isTeacher(), account.isAdmin());
    }

    public Account findByLogin(String login) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM accounts WHERE login = ?", new Object[]{login}, ROW_MAPPER);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public int delete(int id) {
        return jdbcTemplate.update("DELETE FROM accounts WHERE id = ?", id);
    }

    public int delete(Account account) { return delete(account.getId()); }
}